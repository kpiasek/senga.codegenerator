﻿using System;

namespace Common.Attributes
{
    [AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false)]
    public class GenerateAssemblyInfoAttribute : Attribute
    {
    }
}
