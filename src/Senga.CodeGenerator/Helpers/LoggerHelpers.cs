﻿using Serilog;

using System;
using System.IO;

namespace Senga.CodeGenerator.Helpers
{
    /// <summary>
    ///     Klasa statyczna zapewniające obsługę logowania dla generatorów.
    ///     Obsługa oparta jest o Serilog.
    ///     Klasa definiuje domyślny wzorzec wpisu w log.
    ///     Klasa umożliwia również ominięcie problemu, który na dzień dzisiejszy występuje
    ///     w działaniu kompilatora podczas wykorzystywania generatorów kodu źródłowego.
    ///     Manowicie kontekst przekazywany do funkcji Initialize 
    ///     (<see cref="Microsoft.CodeAnalysis.ISourceGenerator.Initialize(Microsoft.CodeAnalysis.GeneratorInitializationContext)"/>)
    ///     nie zawiera informacji o opcjach kompilacji (jest to problem zarejestrowany w zgłoszeniach i podobno
    ///     uzyskał wyskoi priorytet). W tej sytuacji na tym etapie nie ma możliwości ustawienia ścieżki
    ///     dla plików logów. W związku z tym w pierwszej fazie logowanie odbywa się do pamięci, dopiero
    ///     pierwsze przekazanie do klasy ścieżki do pliku logowania powoduje docelowe skonfigurowanie
    ///     logowania i przepisanie zgromadzonych wpisów.
    ///     Ścieżkę można ustawić tylko raz. To nie stanowi problemu, pomimo że klasa jest statyczna,
    ///     a więc współdzielona przez wszystkie generatory, ponieważ kompilator do wszystkich generatorów
    ///     przekazuje ten sam kontekst, a więc również te same opcje.
    /// </summary>
    public static class LoggerHelpers
    {
        private static readonly string _outTemplate =
            "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] {SourceContext}::{Caller}: {Message:lj}{NewLine}{Exception}";

        private static string _logPath;
        private static StringWriter _buffer;

        static LoggerHelpers()
        {
            _buffer = new StringWriter();

            Serilog.Log.Logger = new LoggerConfiguration()
            .WriteTo.TextWriter(
                _buffer,
                outputTemplate: _outTemplate)
            .CreateLogger();
        }

        public static string LogPath
        {
            get => _logPath;
            set
            {
                if (!string.IsNullOrWhiteSpace(_logPath))
                {
                    return;
                }

                if (string.IsNullOrWhiteSpace(value))
                {
                    return;
                }

                string path;
                try
                {
                    path = Path.GetFullPath(value);
                }
                catch (Exception ex)
                {
                    Log
                        .ForContext(typeof(LoggerHelpers))
                        .ForContext("Caller", nameof(LogPath))
                        .Error(ex, $"Błędna ścieżka logowania {value}");
                    return;
                }

                _logPath = value;

                FinalInitLogger();
            }
        }

        public static ILogger Log => Serilog.Log.Logger;

        private static void FinalInitLogger()
        {
            Serilog.Log.CloseAndFlush();

            string path = Path.Combine(_logPath, "log.log");
            Serilog.Log.Logger = new LoggerConfiguration()
                .WriteTo.File(
                    path,
                    rollingInterval: RollingInterval.Day,
                    outputTemplate: "{Message:lj}")
                .CreateLogger();

            Serilog.Log.Logger.Information(_buffer.GetStringBuilder().ToString());
            Serilog.Log.CloseAndFlush();

            Serilog.Log.Logger = new LoggerConfiguration()
                .WriteTo.File(
                    path,
                    rollingInterval: RollingInterval.Day,
                    outputTemplate: _outTemplate)
                .CreateLogger();
        }
    }
}