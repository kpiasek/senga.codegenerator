﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Reflection.Metadata;
using System.Reflection.PortableExecutable;

namespace Senga.CodeGenerator.Helpers
{
    public static class AssemblyResolve
    {
        private static string _dependencyAssemblyPath;
        private static string _nuGetGlobalPackPath;
        private static bool _handlerInstalled;

        internal static string DependencyAssemblyPath
        {
            get
            {
                if (string.IsNullOrEmpty(_dependencyAssemblyPath))
                {
                    _dependencyAssemblyPath = GetDependencyAssemblyPath();
                }

                return _dependencyAssemblyPath;
            }
        }

        internal static string NuGetGlobalPackPath
        {
            get
            {
                if (string.IsNullOrEmpty(_nuGetGlobalPackPath))
                {
                    _nuGetGlobalPackPath = GetNuGetGlobalPackPath();
                }

                return _nuGetGlobalPackPath;
            }
        }

        /// <summary>
        ///     Przeszukuje przekazany katalog zakładając, że jest to katalog NuGet
        ///     zawierający podkatalogi z różymi wersjami poszukiwanego pakietu.
        ///     Porównanie asemblacji opiera się na porównaniu nazwy, a następnie
        ///     porównaniu identyfikatora modułu MDIV, który jest unikalny dla każdej
        ///     kompilacji.
        /// </summary>
        /// <param name="assLoaded">Poszukiwana asemblacja</param>
        /// <param name="inPath">Katalog do przeszukania</param>
        /// <returns>
        ///     Ścieżkę dostępu do katalogu pakietu, jeśli została odnaleziona.
        ///     W przeciwnym razie pusty łańcuch.
        /// </returns>
        public static string FindGeneratorAssemblyInNuGetPath(Assembly assLoaded, string inPath)
        {
            Guid mvidToFind = assLoaded.ManifestModule.ModuleVersionId;
            string nameToFind = Path.GetFileName(assLoaded.Location);

            string[] dirs = Directory.GetDirectories(inPath);

            foreach (string dir in dirs)
            {
                string localpath = Path.Combine(dir, @"analyzers\dotnet\cs", nameToFind);

                if (!File.Exists(localpath))
                {
                    continue;
                }

                using (var fs = new FileStream(
                        localpath,
                        FileMode.Open,
                        FileAccess.Read,
                        FileShare.ReadWrite))
                {
                    using (var peReader = new PEReader(fs))
                    {
                        MetadataReader mr = peReader.GetMetadataReader();

                        Guid mvid = mr.GetGuid(mr.GetModuleDefinition().Mvid);

                        if (mvid == mvidToFind)
                        {
                            return dir;
                        }
                    }
                }
            }

            return string.Empty;
        }

        /// <summary>
        ///     Buduje i zwraca ścieżkę dostępu do zależności.
        ///     Generatory są ładowane za pośrednictwem klasy ShadowCopyAnalyzerAssemblyLoader,
        ///     która przed załadowaniem kopiuje biblioteki do katalogu temp, ale ignoruje
        ///     inne pliki. Z tego powodu konieczne jest odnalezienie oryginalnej
        ///     lokalizacji biblioteki generatora i na tej podstawie zbudowania
        ///     ścieżki dostępu do zależności.
        /// </summary>
        /// <returns>
        ///     Ścieżka dostępu do zależności.
        /// </returns>
        internal static string GetDependencyAssemblyPath()
        {
            Assembly loadedAss = Assembly.GetExecutingAssembly();

            string nugetPath = Path.Combine(NuGetGlobalPackPath, Path.GetFileNameWithoutExtension(loadedAss.Location));
            string origPath = FindGeneratorAssemblyInNuGetPath(loadedAss, nugetPath);

            if (string.IsNullOrEmpty(origPath))
            {
                return string.Empty;
            }

            return Path.Combine(origPath, "assemblies");
        }

        /// <summary>
        ///     Pobiera ścieżkę dostępu do katalogu z pakietami NuGet.
        /// </summary>
        /// <returns>
        ///     Zwraca global-packages
        /// </returns>
        internal static string GetNuGetGlobalPackPath()
        {
            Process proc = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "dotnet",
                    Arguments = "nuget locals global-packages --list",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true,
                    ErrorDialog = false
                }
            };

            if (!proc.Start())
            {
                throw new InvalidOperationException("Cannot get nuget locals global-packages.");
            }

            if (!proc.WaitForExit(1000) && !proc.HasExited)
            {
                if (Debugger.IsAttached)
                {
                    Debugger.Launch();
                }

                proc.Close();
                throw new InvalidOperationException("Timeout for get nuget locals global-packages.");
            }

            if (proc.ExitCode != 0)
            {
                proc.Close();
                throw new InvalidOperationException($"Nuget locals global-packages returns with ExitCode {proc.ExitCode}.");
            }

            string output = proc.StandardOutput.ReadLine();
            proc.Close();

            if (!output.StartsWith("global-packages: "))
            {
                throw new InvalidOperationException("Incorrect format of returned information from nuget locals global-packages.");
            }

            string path = output.Remove(0, "global-packages: ".Length);

            return path;
        }

        internal static void InstallAssemblyResolve()
        {
            if (!_handlerInstalled)
            {
                AppDomain currentDomain = AppDomain.CurrentDomain;

                currentDomain.AssemblyResolve += new ResolveEventHandler(ResolveEventHandler);

                _handlerInstalled = true;
            }
        }

        private static Assembly ResolveEventHandler(object sender, ResolveEventArgs args)
        {
            string path = Path.Combine(DependencyAssemblyPath, args.Name.Substring(0, args.Name.IndexOf(",")) + ".dll");
            path = Path.GetFullPath(path);

            if (!string.IsNullOrEmpty(path) && File.Exists(path))
            {
                return Assembly.LoadFrom(path);
            }

            return null;
        }
    }
}
