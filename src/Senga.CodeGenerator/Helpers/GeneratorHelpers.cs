﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;
using Microsoft.CodeAnalysis.Text;

using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Senga.CodeGenerator.Helpers
{
    public static class GeneratorHelpers
    {
        public static T GetGlobalOptions<T>(AnalyzerConfigOptions globOptions, string askOption, T defValue)
        {
            string opt = $"build_property.{askOption}";

            if (globOptions.TryGetValue(opt, out string optValueStr))
            {
                if (typeof(T).Equals(optValueStr))
                {
                    return (T)(object)optValueStr;
                }

                TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
                if (converter?.CanConvertFrom(typeof(string)) == true)
                {
                    return (T)converter.ConvertFromString(optValueStr);
                }
            }

            return defValue;
        }

        public static void AddSource(GeneratorExecutionContext context, string hint, string className, string source)
        {
            AnalyzerConfigOptions globalOpt = context.AnalyzerConfigOptions.GlobalOptions;

            bool shouldSave = GetGlobalOptions(globalOpt, "SaveSourceGeneratorSources", false);

            string savePath = string.Empty;

            if (shouldSave)
            {
                savePath = GetGlobalOptions(globalOpt, "SourceGeneratorSourcesDir", string.Empty);
                if (!Path.IsPathRooted(savePath))
                {
                    shouldSave = false;
                }
            }

            if (shouldSave)
            {
                Directory.CreateDirectory(savePath);
                string path = Path.Combine(savePath, $"{className}.{hint}.cs");
                File.WriteAllText(path, source, Encoding.UTF8);
            }

            context.AddSource($"{className}.{hint}", SourceText.From(source, Encoding.UTF8));
        }

        public static string GetResourceContent(string relativePath)
        {
            var baseName = Assembly.GetExecutingAssembly().GetName().Name;
            var resourceName = relativePath
                .TrimStart('.')
                .Replace(Path.DirectorySeparatorChar, '.')
                .Replace(Path.AltDirectorySeparatorChar, '.');

            using (Stream stream = Assembly.GetExecutingAssembly()
                .GetManifestResourceStream(baseName + "." + resourceName))
            {
                if (stream == null)
                {
                    throw new ArgumentException($"Embedded resource '{relativePath}' not found.");
                }

                using (StreamReader reader = new StreamReader(stream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public static bool IsEqual(this AssemblyName assName, AssemblyName assNameOther)
        {
            return assName.Name == assNameOther.Name &&
                assName.Version == assNameOther.Version &&
                (assName.CultureInfo ??
                    System.Globalization.CultureInfo.InvariantCulture).LCID == assNameOther.CultureInfo.LCID &&
                Enumerable.SequenceEqual(
                    assName.GetPublicKeyToken() ?? Array.Empty<byte>(), assNameOther.GetPublicKeyToken());
        }
    }
}
