﻿using Serilog;

using System.Runtime.CompilerServices;

namespace Senga.CodeGenerator.Helpers
{
    /// <summary>
    ///     Klasa bazowa dostarczająca obsługę logowania.
    ///     Wykorzystuje statyczną klasę <see cref="LoggerHelpers"/>.
    ///     Klasa tworzy SourceContext dla klasy logowania przez klasę
    ///     dziedziczącą. Dodatkowo udostępnia funkcję ułatwiającą stworzenie
    ///     kontekstu logowania z informacją o funkcji żądającej wykonania wpisu.
    ///     Właściwość <see cref="Log"/> sprawdza, czy nie zmieniła się statyczna
    ///     właściwość <see cref="LoggerHelpers.Log"/>. Jeżeli doszło do zmiany -
    ///     co ma miejsce w momencie zmiany mechanizmu logowania z logowania do
    ///     pamięci na logowanie do pliku <see cref="LoggerHelpers"/> - powtórnie
    ///     tworzony jest nowy SourceContext.
    /// </summary>
    public class LogBase
    {
        private ILogger _logSaved;
        private ILogger _log;

        public LogBase()
        {
            _logSaved = LoggerHelpers.Log;
            _log = LoggerHelpers.Log?.ForContext(GetType());
        }

        protected ILogger Log
        {
            get
            {
                if (ReferenceEquals(_logSaved, LoggerHelpers.Log))
                {
                    return _log;
                }

                _logSaved = LoggerHelpers.Log;
                _log = LoggerHelpers.Log?.ForContext(GetType());

                return _log;
            }
        }

        protected ILogger GetLogForCaller([CallerMemberName] string callerName = "")
        {
            return Log.ForContext("Caller", callerName);
        }
    }
}
