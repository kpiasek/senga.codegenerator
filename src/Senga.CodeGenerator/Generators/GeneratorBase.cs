﻿using Microsoft.CodeAnalysis;

using Senga.CodeGenerator.Helpers;

using System.Diagnostics;

namespace Senga.CodeGenerator.Generators
{
    /// <summary>
    ///     Abstrakcyjna klasa której podstawowym zadaniem jest poprawne wykorzystanie mechanizmów
    ///     <see cref="LogBase"/>.
    ///     Istnieją tutaj dwa aspekty.
    ///     Pierwszy to odczytania ścieżki logowania z przekazanych ustawień kompilacji i przekazanie
    ///     jej do <see cref="LoggerHelpers.LogPath"/> co gdy wydarzy się po raz pierwszy od
    ///     załadowania asemblacji spowoduje zmianę sposobu logowania z logowania do pamięci na
    ///     logowanie do pliku (<see cref="LoggerHelpers"/>).
    ///     Drugi ma związek ze sposobem ładowania generatorów do pamięci. Ponieważ sposób w jaki
    ///     generatory są wykorzystywane wyklucza użycie referencji projektu, wszystkie binaria muszą
    ///     być dostarczone wraz z generatorem i w scenariuszu bazowym być w jednym katalogu z podstawową
    ///     biblioteką. To powoduje, że wszystkie asemblacje pojawią się w projekcie wykorzystującym
    ///     generator w zależnościach (Dependencies/Analyzer). Mało tego kompilator wszystkie te biblioteki
    ///     będzie sprawdzał na okoliczność występowania klas z atrybutem [Generator]. W związku z powyższym
    ///     zdecydowałem się na umieszczanie wszystkich zależności w odrębnym katalogu, który nie jest
    ///     podkatalogiem katalogu Analyzers oraz zaimplementowanie statycznej klasy <see cref="AssemblyResolve"/>
    ///     dostarczającej obsługę zdarzenia <see cref="System.AppDomain.AssemblyResolve"/> w celu załadowania
    ///     bibliotek. Obsługa zdarzenia musi zostać zainstalowana zanim dojdzie do próby załadowania
    ///     jakiegokolwiek niestandardowego typu, co spowoduje błąd nieodnalezienia biblioteki lub jej 
    ///     zależności. Obsługa instalowana jest w konstruktorze statycznym, który w takiej konfiguracji
    ///     wykonywany jest najwcześniej. Gdyby w tej klasie znalazły się definicje pól lub właściwości
    ///     wykorzystujących niestandardowe biblioteki (np. logowania) ładowanie nie powiodło by się.
    ///     W sytuacji, gdy tego typu implementacja została wyniesiona do klasy bazowej problem nie istnieje.
    ///     Należy jednak pamiętać, że kluczowe jest wykorzystanie konstruktora statycznego.
    ///     Wykorzystanie abstrakcyjnej klasy i imlementacji explicite interfejsu pozwala zachować prawie
    ///     niezmienioną w stosunku do podstawowej implementację właściwego generatora. Różnica polega
    ///     jedynie na dodaniu klasy bazowej <see cref="GeneratorBase"/> oraz słów kluczowych override
    ///     do metod Initialize oraz Execute.
    /// </summary>
    public abstract class GeneratorBase : LogBase, ISourceGenerator
    {
        static GeneratorBase()
        {
            AssemblyResolve.InstallAssemblyResolve();
        }

        public abstract void Initialize(GeneratorInitializationContext context);

        public abstract void Execute(GeneratorExecutionContext context);

        void ISourceGenerator.Initialize(GeneratorInitializationContext context)
        {
            Initialize(context);
        }

        void ISourceGenerator.Execute(GeneratorExecutionContext context)
        {
            LoggerHelpers.LogPath =
                GeneratorHelpers
                    .GetGlobalOptions(context.AnalyzerConfigOptions.GlobalOptions, "SourceGeneratorSourcesDir", "");

            Execute(context);
        }
    }
}
