﻿using Common.Attributes;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp.Syntax;

using System.Collections.Generic;
using System.Linq;

namespace Senga.CodeGenerator.Generators
{
    internal class EqualsImplSyntaxReceiver : ISyntaxReceiver
    {
        public IList<ClassDeclarationSyntax> CandidateClasses { get; } = new List<ClassDeclarationSyntax>();

        public void OnVisitSyntaxNode(SyntaxNode syntaxNode)
        {
            if (syntaxNode is ClassDeclarationSyntax cds &&
                cds.AttributeLists.Any(x =>
                    x.Attributes.Any(a =>
                        a.Name.ToString().Equals(
                            nameof(EqualsImplAttribute).Replace("Attribute", string.Empty)))))
            {
                CandidateClasses.Add(cds);
            }
        }
    }
}