﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

using Common.Attributes;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using Microsoft.CodeAnalysis.Text;

using Scriban;
using Scriban.Runtime;

using Senga.CodeGenerator.Helpers;

namespace Senga.CodeGenerator.Generators
{
    [Generator]
    public class EqualsImplGenerator : GeneratorBase
    {
        public override void Initialize(GeneratorInitializationContext context)
        {
            GetLogForCaller().Information("Initialize");

            context.RegisterForSyntaxNotifications(() => new EqualsImplSyntaxReceiver());
        }

        public override void Execute(GeneratorExecutionContext context)
        {
            try
            {
                GetLogForCaller().Information("Start execute.");

                if (GeneratorHelpers.GetGlobalOptions(
                    context.AnalyzerConfigOptions.GlobalOptions, "DebugSourceGenerators", false))
                {
                    if (!Debugger.IsAttached)
                    {
                        Debugger.Launch();
                    }
                }

                string templateText = GeneratorHelpers.GetResourceContent(@"Resources\EqualsImpl.sbntmpl");
                Template template = Template.Parse(templateText);

                GetLogForCaller().Information("EqualsImpl template loaded.");

                EqualsImplSyntaxReceiver cfei = (EqualsImplSyntaxReceiver)context.SyntaxReceiver;

                GetLogForCaller().Information($"{cfei.CandidateClasses.Count} candidates classes has been identified.");

                foreach (ClassDeclarationSyntax cds in cfei.CandidateClasses)
                {
                    GetLogForCaller().Information($"Start of processing {cds.Identifier} class.");

                    if (!cds.Modifiers.Any(SyntaxKind.PartialKeyword))
                    {
                        context.ReportDiagnostic(
                            Diagnostic.Create(new DiagnosticDescriptor(
                                    "CG1000",
                                    "Missing partial modifier",
                                    "Class {0} with attribute [EqualsImpl] have to mark as partial.",
                                    "CodeGenerator.Requirements",
                                    DiagnosticSeverity.Error,
                                    true,
                                    "For class with attribute [EqualsImpl] should be generated partial class with " +
                                    "implementation of interface IEquatable, so class have to mark as partial.",
                                    null,
                                    WellKnownDiagnosticTags.Build),
                                cds.GetLocation(),
                                cds.Identifier));

                        GetLogForCaller()
                            .Warning($"No partial keyword - end of processing {cds.Identifier} class.");

                        continue;
                    }

                    SourceText sourcetext = cds.SyntaxTree.GetText();
                    SemanticModel semmodel = context.Compilation.GetSemanticModel(cds.SyntaxTree);

                    ScriptObject model = new ScriptObject();

                    model["modifiers"] = sourcetext.GetSubText(cds.Modifiers.Span);

                    model["namespace"] = cds
                        .Ancestors()
                        .OfType<NamespaceDeclarationSyntax>()
                        .Single()?
                        .Name
                        .ToString();

                    model["classname"] = cds.Identifier +
                        (cds.TypeParameterList != null ?
                            sourcetext.GetSubText(cds.TypeParameterList.Span).ToString() :
                            string.Empty);

                    List<object> properties = new List<object>();

                    int propsEval = 0;

                    foreach (PropertyDeclarationSyntax pds in
                        cds.Members.Where(m => m.Kind() == SyntaxKind.PropertyDeclaration))
                    {
                        if (pds.AttributeLists
                            .Any(x => x.Attributes
                                .Any(a => a.Name.ToString().Equals(
                                    nameof(EqualsImplIgnoreAttribute).Replace("Attribute", string.Empty)))))
                        {
                            continue;
                        }

                        propsEval++;

                        var type = semmodel.GetTypeInfo(pds.Type);
                        if (type.Type.SpecialType == SpecialType.None &&
                            type.Type.AllInterfaces.Any(i => i.Name == nameof(IEnumerable)))
                        {
                            properties.Add(new { name = pds.Identifier.ValueText, type = "sequence" });
                        }
                        else
                        {
                            properties.Add(new { name = pds.Identifier.ValueText, type = "normal" });
                        }
                    }

                    if (propsEval == 0)
                    {
                        context.ReportDiagnostic(
                            Diagnostic.Create(new DiagnosticDescriptor(
                                    "CG1001",
                                    "No properties to compare",
                                    "Class {0} has not properties to compare. Check EqualsImplIgnore attributes.",
                                    "CodeGenerator.Requirements",
                                    DiagnosticSeverity.Warning,
                                    true,
                                    "For class with attribute [EqualImpl] should be generated partial class with " +
                                    "implementation of interface IEquatable, so class have to properties to compare.",
                                    null,
                                    WellKnownDiagnosticTags.Build),
                                cds.GetLocation(),
                                cds.Identifier));

                        GetLogForCaller()
                            .Information($"No properties to compare - end of processing {cds.Identifier} class.");

                        continue;
                    }

                    model["properties"] = properties;

                    string source = template.Render(model);

                    GeneratorHelpers.AddSource(
                        context, "EqualsImpl", cds.Identifier.ToString(), source);

                    GetLogForCaller()
                        .Information($"Source was generated - end of processing {cds.Identifier} class.");
                }
            }
            catch(Exception ex)
            {
                GetLogForCaller().Error(ex, "Error during source generating.");
            }
        }
    }
}