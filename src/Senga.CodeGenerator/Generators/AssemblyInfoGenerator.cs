﻿using Common.Attributes;

using LibGit2Sharp;

using Microsoft.CodeAnalysis;

using Senga.CodeGenerator.Helpers;

using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Senga.CodeGenerator.Generators
{
    [Generator]
    public class AssemblyInfoGenerator : GeneratorBase
    {
        public override void Initialize(GeneratorInitializationContext context)
        {
            GetLogForCaller().Information("Initialize");
        }

        public override void Execute(GeneratorExecutionContext context)
        {
            GetLogForCaller().Information("Start execute.");

            if (GeneratorHelpers.GetGlobalOptions(
                   context.AnalyzerConfigOptions.GlobalOptions, "DebugSourceGenerators", false))
            {
                if (!Debugger.IsAttached)
                {
                    Debugger.Launch();
                }
            }

            var hasAttrib =
                context
                    .Compilation
                    .Assembly
                    .GetAttributes()
                    .Any(x => x.AttributeClass.Name == nameof(GenerateAssemblyInfoAttribute));

            if (!hasAttrib)
            {
                GetLogForCaller().Information($"Assembly doesn't decorate by {nameof(GenerateAssemblyInfoAttribute)}.");
                return;
            }

            var asmName = context.Compilation.AssemblyName;

            // Biblioteka LibGit2Sharp posiada wbudowany mechanizm wspierający ładowanie 
            // natywnych bibliotek. Ustawiam statyczną właściwość NativeLibraryPath
            // klasy GlobalSettings na ścieżkę zawierającą natywne asemblacje.
            // LibGit2Sharp w przypadku wykrycia, że asemblacja wywołująca pracuje
            // w .Net Framework automatycznie rozszerza ścieżkę o 'x64' lub 'x86'.
            string nativepath = Path.Combine(AssemblyResolve.GetDependencyAssemblyPath(), "runtimes");

            try
            {
                GlobalSettings.NativeLibraryPath = nativepath; // Path.Combine(nativepath, "native");
            }
            catch(Exception ex)
            {
                // GlobalSettings.NativeLibraryPath set wywołuje wyłącznie wyjątki LibGit2SharpException.
                // Niestety w przypadku wyjątków pochodzących z wykorzystywanych bibliotek setter 
                // przechwytuje je i wywołuje swój wyjątek bez zachowywania stosu wywołań oraz ustawiania
                // właściwości InnerException.
                // Wyjątki mogą być wywołane gdy:
                // - dochodzi do próby ustawienia NativeLibraryPath dla platformy innej niż .NET Framework lub .NET Core
                // - biblioteki natywane już zostałe załadowane
                // - Path.GetFullPath spowodowało wywołanie wyjątku
                // Pierwszy przypadek nie może wystąpić, drugi nie stanowi problemu, trzeci nie powinien mieć miejsca
                // jeżeli ścieżka jest prawidłowa. Jeżeli nie, to pierwsza próba wywołania funkcji używającej
                // biblioteki natywnej spowoduje wyjątek.
            }

            string path = GeneratorHelpers
                .GetGlobalOptions<string>(context.AnalyzerConfigOptions.GlobalOptions, "ProjectDir", null);

            string savedPath = path;
            Repository repo = null;
            if (path != null)
            {
                try
                {
                    path = Repository.Discover(path);

                    if (path == null)
                    {
                        GetLogForCaller().Warning($"Missing the Git repository. Discover failed on path: {savedPath}.");
                        context.ReportDiagnostic(
                               Diagnostic.Create(new DiagnosticDescriptor(
                                       "CG2000",
                                       "Missing Git repository.",
                                       "Assembly {0} project's folder is not Git repository.",
                                       "CodeGenerator.Requirements",
                                       DiagnosticSeverity.Error,
                                       true,
                                       "For assembly with attribute [GenerateAssemblyInfo] should be generated " +
                                       "static class with information collected from Git repository " +
                                       "so project has to be part of repository.",
                                       null,
                                       WellKnownDiagnosticTags.Build),
                                   null,
                                   asmName));

                        return;
                    }

                    repo = new Repository(path);
                }
                catch (Exception ex)
                {
                    GetLogForCaller().Fatal(ex,
                        $"Create Repository Object failed. Native library path: {GlobalSettings.NativeLibraryPath}.");
                    context.ReportDiagnostic(
                           Diagnostic.Create(new DiagnosticDescriptor(
                                   "CG2002",
                                   "Create Repository Object failed.",
                                   "Error while creating the Repository Object.",
                                   "CodeGenerator.Requirements",
                                   DiagnosticSeverity.Error,
                                   true,
                                   "Error while creating the Repository Object. " +
                                   $"Native library path: {GlobalSettings.NativeLibraryPath}. " +
                                   "Details can be found in the log file.",
                                   null,
                                   WellKnownDiagnosticTags.Build),
                               null,
                               asmName));

                    return;
                }
            }

            GetLogForCaller().Information("Access to the Git repository has been established.");

            DateTime date = DateTime.Now.ToUniversalTime();

            StringBuilder strb = new StringBuilder();
            strb.Append("public static class ").AppendLine("AssemblyInfo");
            strb.AppendLine("{");

            strb.Append("    public const string NumberOfCommits = \"")
                .Append(repo.Commits.Count())
                .AppendLine("\";");
            strb.Append("    public const string UserName = \"")
                .Append(repo.Config.Get<string>("user.name").Value ?? "String.Empty")
                .AppendLine("\";");
            strb.Append("    public const string UserMail = \"")
                .Append(repo.Config.Get<string>("user.email").Value ?? "String.Empty")
                .AppendLine("\";");
            strb.Append("    public const string ProjectUrl = \"")
                .Append(repo.Config.Get<string>("remote.origin.url").Value ?? "String.Empty")
                .AppendLine("\";");
            strb.Append("    public const string HeadFriendlyName = \"")
                .Append(repo.Head.FriendlyName)
                .AppendLine("\";");
            strb.Append("    public const string HeadShortSha = \"")
                .Append(repo.Head.Tip.Sha, 0, 7)
                .AppendLine("\";");
            strb.Append("    public const string HeadSha = \"")
                .Append(repo.Head.Tip.Sha)
                .AppendLine("\";");
            strb.Append("    public const string CompilationTimeStamp = \"")
                .Append(date.ToString())
                .AppendLine("\";");
            strb.Append("    public const string CompilationTimeStampYear = \"")
                .Append(date.Year)
                .AppendLine("\";");
            strb.Append("    public const string CompilationTimeStampMonth = \"")
                .Append(date.Month)
                .AppendLine("\";");
            strb.Append("    public const string CompilationTimeStampDay = \"")
                .Append(date.Day)
                .AppendLine("\";");
            strb.Append("    public const string CompilationTimeStampHour = \"")
                .Append(date.Hour)
                .AppendLine("\";");
            strb.Append("    public const string CompilationTimeStampMinute = \"")
                .Append(date.Minute)
                .AppendLine("\";");
            strb.Append("    public const string CompilationTimeStampSecond = \"")
                .Append(date.Second)
                .AppendLine("\";");
            strb.AppendLine("}");

            GeneratorHelpers.AddSource(context, "AssemblyInfo", "AssemblyInfo", strb.ToString());

            GetLogForCaller()
                       .Information("Source was generated - end of processing AssemblyInfo class.");
        }
    }
}
