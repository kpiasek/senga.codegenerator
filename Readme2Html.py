import markdown

with open('Readme.md', 'r') as f:
    text = f.read()
    html = markdown.markdown(text, extensions=['fenced_code', 'codehilite'])

with open('Readme.html', 'w') as f:
    f.write(html)