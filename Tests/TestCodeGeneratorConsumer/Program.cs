﻿using CodeAnalysis;

using Common.Attributes;

using System;

[assembly: GenerateAssemblyInfo]

namespace CodeGeneratorConsumer
{
    public class Program
    {
        static public void Main(string[] args)
        {
            SimpleClass sc1 = new ()
            {
                Id = 100,
                Name = "First"
            };

            SimpleClass sc2 = new ()
            {
                Id = 100,
                Name = "First"
            };

            if (sc1 == sc2)
            {
                Console.WriteLine("Obiekty są równe!");
            }
            else
            {
                Console.WriteLine("Obiekty nie  są równe!");
            }
        }
    }
}
