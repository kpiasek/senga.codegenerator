﻿using Common.Attributes;

namespace CodeAnalysis
{
    [EqualsImpl]
    public partial class SimpleClass
    {
        public string Name { get; set; }
        public int Id { get; set; }
    }
}
