using Common.Attributes;

using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.CSharp;
using Microsoft.CodeAnalysis.Text;

using NUnit.Framework;

using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Reflection;

using TestCodeGenerators.Helpers;

using System.Runtime.CompilerServices;

namespace TestCodeGenerators
{
    public class CodeGenerators_Test
    {
        private const string _source =
@"using System.Collections.Generic;

using Common.Attributes;

[assembly: GenerateAssemblyInfo]

namespace CodeAnalysis
{
    [EqualsImpl]
    public partial class SimpleClass
    {
        public string Name { get; set; }
        [EqualsImplIgnore]
        public int Id { get; set; }
        public List<int> List { get; set; } = new List<int>();

        public void AddToList(int element)
        {
            List.Add(element);
        }
    }
}";

        [TestCase("Senga.CodeGenerator", "Debug", _source)]
        public void Generators_test(string genprojname, string conf, string source)
        {
            Assembly asm = CompileAndLoadAssembly(genprojname, conf, source);

            Type t = asm.GetType("CodeAnalysis.SimpleClass");
            object obj1 = Activator.CreateInstance(t);
            t.GetProperty("Name").GetSetMethod().Invoke(obj1, new object[] { "Test1" });
            t.GetProperty("Id").GetSetMethod().Invoke(obj1, new object[] { 111 });
            t.GetMethod("AddToList").Invoke(obj1, new object[] { 123 });

            object obj2 = Activator.CreateInstance(t);
            t.GetProperty("Name").GetSetMethod().Invoke(obj2, new object[] { "Test1" });
            t.GetProperty("Id").GetSetMethod().Invoke(obj2, new object[] { 111 });
            t.GetMethod("AddToList").Invoke(obj2, new object[] { 123 });

            MethodInfo miEquals = t.GetMethod("Equals", new Type[] { typeof(object) });

            object res = miEquals.Invoke(obj1, new object[] { obj2 });
            Assert.IsTrue((bool)res);

            t.GetProperty("Name").GetSetMethod().Invoke(obj2, new object[] { "Test2" });
            res = miEquals.Invoke(obj1, new object[] { obj2 });
            Assert.IsFalse((bool)res);

            Type ai = asm.GetType("AssemblyInfo");
            string url = ai.GetField("ProjectUrl").GetRawConstantValue() as string;
            Assert.IsNotEmpty(url);
        }

        private Assembly CompileAndLoadAssembly(string genprojname, string conf, string source)
        {
            // Utworzenie SyntaxTree z kodu �r�d�owego przekazanego w �a�cuchu znakowym.
            SyntaxTree syntree = CSharpSyntaxTree.ParseText(SourceText.From(source));

            // W momencie tworzenia SyntaxTree sprawdzane s� b��dy sk�adniowe.
            List<Diagnostic> diag = syntree.GetDiagnostics().ToList();
            Assert.Zero(diag.Count);

            // �cie�ka dost�pu do bibliotek refrencyjnych. VS dodaje je automatycznie do projektu.
            // Uwaga: wersja �adowanych bibliotek musi by� zgodna z wersj� �rodowiska projektu testowego!
            // W przeciwnym razie kompilacja zako�czy si� powodzeniem, ale Assembly.Load ju� nie.
            string refdir = @"C:\Program Files\dotnet\packs\Microsoft.NETCore.App.Ref\5.0.0\ref\net5.0";

            // Utworzenie listy obiekt�w PortableExecutableReference z bibliotek znajduj�cych si� 
            // w katalogu referencji. Konieczne jest pomini�cie plik�w innych ni� .dll, np. .xml.
            var refs = Directory
                .GetFiles(refdir)
                .Where(f => Path.GetExtension(f) == ".dll")
                .Select(f => MetadataReference.CreateFromFile(f));

            // Tworzenie kompilacji AssemblyInMemmory dla biblioteki klas (OutputKind.DynamicallyLinkedLibrary).
            // Do kompilacji dodawane s�: utworzony wcze�niej obiekt SyntaxTree, przygotowane referencje
            // oraz referencje do testowanych bibliotek.
            CSharpCompilation compilation = CSharpCompilation.Create("AssemblyInMemmory")
                .WithOptions(new CSharpCompilationOptions(OutputKind.DynamicallyLinkedLibrary))
                    .AddSyntaxTrees(syntree)
                    .AddReferences(refs)
                    .AddReferences(
                        MetadataReference.CreateFromFile(typeof(EqualsImplAttribute).Assembly.Location));

            // Kompilacja jest aktualizowana przez obiekt EqualsImplGenerator. W rezultacie jest tworzony
            // nowy obiekt kompilacji oraz lista z komunikatami diagnostycznymi.
            // Aby przetestowa� mo�liwie identyczn� sytuacj�, z t� kt�ra ma miejsce podczas normalnego 
            // wykorzystywania generatora przed rzopocz�ciem test�w pakiet generatora powinien znale�� si�
            // w lokalnym katalogu przechowuj�cym zainstalowane pakiety NuGet. Mo�na to osi�gn�� na przyk�ad
            // instaluj�c pakiet w projekcie TestCodeGeneratorConsumer.
            // Poniewa� Roslyn do za�adowania bibliotek generator�w u�ywa klasy ShadowCopyAnalyzerAssemblyLoader,
            // kt�ra kopiuje bibliotek� do katalogu tymczasowego i stamt�d j� uruchamia, konieczne jest
            // powt�rzenie podonego zabiegu, aby m�c przetestowa� mechanizm �adowania zale�no�ci.
            // Biblioteka generator�w kopiowana jest z katalogu pakiet�w NuGet do katalogu tymczasowego.
            string slnpath = TestHelpers.GetSolutionPath();
            string gentestpath = Path.Combine(
               slnpath,
               @$"src\{genprojname}\bin\{conf}\netstandard2.0\{genprojname}.dll");
            string genloadpath = Path.Combine(Path.GetTempPath(), Path.GetFileName(gentestpath));
            File.Copy(gentestpath, genloadpath, true);

            // Poni�szy fragment wykorzystuje metody zadeklarowane poni�ej z atrybutem NoInlining.
            // Metoda LoadContextAndCompile wykorzystuje testowy kontekst �adowania, co teoretycznie
            // pozwala na usuni�cie wraz z kontekstem za�adowanych asemblacji. Po usuni�ciu powinno
            // by� mo�liwe usuni�cie z katalogu tymczasowego kopii biblioteki.
            // Fagmenty kodu umieszczone w funkcjach powinny pozwoli� pom�c grabberowi uwolni� pami��
            // (zmienne lokalne). Zabieg jest konieczny, poniewa� tylko po usuni�cie z pami�ci
            // wszystkich referencji jest mo�liwe usuni�cie kontekstu �adowania. Do sprawdzania
            // czy kontekst zosta� usuni�ty s�u�y zmienna typu WeakReference. Niestety, je�eli
            // dochodzi do stworzenia obiekt�w opartych o klasy zdefiniowane w bibliotece generatora
            // nie dochodzi do usuni�cia z kontekstu z pami�ci, a co za tym idzie niemo�liwe jest 
            // usuni�cie biblioteki z dysku. W toku test�w okaza�o si�, �e nawet w sytuacji, w kt�rej
            // kontekst zosta�a usuni�ty (generatory nie zostay utworzone i u�yte), usuni�cie pliku
            // i tak nie jest mo�liwe. Opis tego typu problemu powtarza si� r�wnie� na forach.
            MemoryStream stream;
            WeakReference weakContextRef;
            LoadContextAndCompile(genloadpath, compilation, out stream, out weakContextRef);
            WaiForUnloadContext(weakContextRef);

            // Pr�ba usuni�cia pliku sko�czy si� wyj�tkiem: brak dost�pu, plik zablokowany przez inn� aplikacj�.
            try
            {
                File.Delete(genloadpath);
            }
            catch
            { }

            // Za�adowanie utworzonej asemblacji i test implementacji interfejsu IEquatable
            // utworzonej przez generator EqualsImplGenerator.
            Assembly asm = Assembly.Load(stream.ToArray());

            return asm;
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private void LoadContextAndCompile(string genloadpath, CSharpCompilation compilation, out MemoryStream stream, out WeakReference weakReference)
        {
            TestAssemblyLoadContext localContext = new();
            Assembly genass = localContext.LoadFromAssemblyPath(genloadpath);
            weakReference = new(localContext, true);

            // Nale�y pami�ta�, �e specyfika LINQ powoduje, i� w tym miesjcu nie dochodzi ani do tworzenia
            // nowej kolekcji, ani do wywo�ania metod CreateInstance. Zostaje zapisany w pami�ci niejako
            // przepis, kt�ry zostanie wykonany dopiero w razie potrzeby.
            var gentypes =
                genass.DefinedTypes
                    .Where(dt => dt.CustomAttributes.Any(ca => ca.AttributeType == typeof(GeneratorAttribute)));
            var genobjs = gentypes.Select(t => (ISourceGenerator)Activator.CreateInstance(t));

            CSharpGeneratorDriver gendriver = CSharpGeneratorDriver.Create(
               genobjs,
               optionsProvider: new AnalyzerConfigOptionsProviderForTest());
            GeneratorDriver gd = gendriver.RunGeneratorsAndUpdateCompilation(
               compilation, out Compilation compout, out ImmutableArray<Diagnostic> diagout);

            Assert.Zero(diagout.Length);

            // Wykonanie kompilacji. Je�li si� powiedzie wynikowa asemblacja znajdzie si� w strumieniu
            // w pami�ci.
            stream = new();
            var emitResult = compout.Emit(stream);
            localContext.Unload();
            Assert.IsTrue(emitResult.Success);
        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        private void WaiForUnloadContext(WeakReference weakReference)
        {
            // Ze wzgl�du na spos�b dzia�ania grabbera konieczne mo�e by� kilkukrotne wywo�anie
            // GC.Collect w celu wyczyszczenia pami�ci. Dzieje si� tak zar�wno ze wzgl�du na 
            // mechanizmy wielow�tkowo�ci, jak i spos�b wykorzystywania przez grabber finalizator�w.
            // W wi�kszo�ci przypadk�w referencje, kt�re maj� by� usuni�te zostaj� usuni�te w najwy�ej
            // dw�ch wywo�aniach. 10 iteracji jest powt�rzone za przyk�adami Microsoftu.
            int i = 10;
            while (weakReference.IsAlive && i-- > 0)
            {
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
        }
    }
}