﻿using Microsoft.CodeAnalysis;
using Microsoft.CodeAnalysis.Diagnostics;

namespace TestCodeGenerators.Helpers
{
    /// <summary>
    ///     Klasa dostawcy opcji kompilacji. Wykorzystuje <see cref="AnalyzerConfigOptionsForTest"/>.
    /// </summary>
    public class AnalyzerConfigOptionsProviderForTest : AnalyzerConfigOptionsProvider
    {
        private readonly AnalyzerConfigOptionsForTest _options = new AnalyzerConfigOptionsForTest();

        public override AnalyzerConfigOptions GlobalOptions => _options;

        public override AnalyzerConfigOptions GetOptions(SyntaxTree tree)
        {
            return _options;
        }

        public override AnalyzerConfigOptions GetOptions(AdditionalText textFile)
        {
            return _options;
        }
    }
}
