﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Reflection.Metadata;
using System.Reflection.PortableExecutable;

namespace TestCodeGenerators.Helpers
{
    public static class TestHelpers
    {
        /// <summary>
        ///     Zwraca ścieżkę solucji pod warunkiem, że bazowy katalog testów
        ///     nazywa się Tests i jest bezpośednim podkatalogiem głównego
        ///     katalogu solucji.
        /// </summary>
        /// <returns>
        ///     Katalog solucji.
        /// </returns>
        public static string GetSolutionPath()
        {
            string path = NUnit.Framework.TestContext.CurrentContext.TestDirectory;

            string subpath = @"\Tests\";

            if (!path.Contains(subpath))
            {
                throw new FormatException($"Unexpected path format, {subpath} subfolder not found.");
            }

            return Path.GetDirectoryName(path.Remove(path.IndexOf(subpath) + subpath.Length - 1));
        }

        /// <summary>
        ///     Pobiera ścieżkę dostępu do katalogu z pakietami NuGet.
        /// </summary>
        /// <returns>
        ///     Zwraca global-packages
        /// </returns>
        public static string GetNuGetGlobalPackPath()
        {
            Process proc = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "dotnet",
                    Arguments = "nuget locals global-packages --list",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };

            proc.Start();

            if (!proc.WaitForExit(1000))
            {
                throw new InvalidOperationException("Timeout for get nuget locals global-packages.");
            }

            if (proc.ExitCode != 0)
            {
                throw new InvalidOperationException($"Nuget locals global-packages returns with ExitCode {proc.ExitCode}.");
            }

            string output = proc.StandardOutput.ReadLine();

            if (!output.StartsWith("global-packages: "))
            {
                throw new InvalidOperationException("Incorrect format of returned information from nuget locals global-packages.");
            }

            string path = output.Remove(0, "global-packages: ".Length);

            return path;
        }

        public static string FindAssemblyInNuGetPath(AssemblyName assNameToFind, string inPath)
        {
            string[] dirs = Directory.GetDirectories(inPath);

            // Tablica dirs zawiera ścieżki dostępu do poszczególnych zainstalowanych wersji
            // pakietu z poszukiwaną biblioteką. Ostatni człon ścieżki jest oznaczeniem wersji.
            var vers = dirs.Select(d => new Version(Path.GetFileName(d))).OrderByDescending(d => d);

            string result = string.Empty;
            foreach (Version v in vers)
            {
                if (!string.IsNullOrEmpty(result))
                {
                    break;
                }

                string localpath = Path.Combine(inPath, v.ToString(), @"analyzers\dotnet\cs");

                foreach (string f in Directory.EnumerateFiles(localpath, "*.dll"))
                {
                    using (var fs = new FileStream(
                        f,
                        FileMode.Open,
                        FileAccess.Read,
                        FileShare.ReadWrite))
                    {
                        using (var peReader = new PEReader(fs))
                        {
                            MetadataReader mr = peReader.GetMetadataReader();
                            AssemblyName assname = mr.GetAssemblyDefinition().GetAssemblyName();

                            if (assname.IsEqual(assNameToFind))
                            {
                                result = Path.Combine(inPath, v.ToString());
                                break;
                            }
                        }
                    }
                }
            }

            return result;
        }

        public static bool IsEqual(this AssemblyName assName, AssemblyName assNameOther)
        {
            return assName.Name == assNameOther.Name &&
                assName.Version == assNameOther.Version &&
                (assName.CultureInfo ??
                    System.Globalization.CultureInfo.InvariantCulture).LCID == assNameOther.CultureInfo.LCID &&
                Enumerable.SequenceEqual(
                    assName.GetPublicKeyToken() ?? Array.Empty<byte>(), assNameOther.GetPublicKeyToken());
        }
    }
}
