﻿using Microsoft.CodeAnalysis.Diagnostics;

using System.Diagnostics.CodeAnalysis;
using System.IO;

namespace TestCodeGenerators.Helpers
{
    /// <summary>
    ///     Klasa dostarczająca opcji kompilacji do testu.
    /// </summary>
    public class AnalyzerConfigOptionsForTest : AnalyzerConfigOptions
    {
        public override bool TryGetValue(string key, [NotNullWhen(true)] out string value)
        {
            switch(key)
            {
                case "build_property.ProjectDir":
                    value = TestHelpers.GetSolutionPath();
                    return true;
                case "build_property.SourceGeneratorSourcesDir":
                    value = Path.Combine(NUnit.Framework.TestContext.CurrentContext.TestDirectory, "Generated");
                    return true;
                case "build_property.SaveSourceGeneratorSources":
                    value = "true";
                    return true;
                default:
                    value = null;
                    return false;
            }
        }
    }
}
