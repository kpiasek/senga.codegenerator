﻿[English version](https://translate.google.com/translate?hl=en&sl=pl&tl=en&u=https://kpiasek.gitlab.io/senga.codegenerator/Readme.html) - machine translation!

# Senga.CodeGenerator

Solucja jest próbą stworzenia w pełni produktywnego szablonu generatora kodu źródłowego z założeniem, że ma być publikowany, jako pakiet NuGet.

## Roslyn Source Generator

[*Roslyn*](https://docs.microsoft.com/dotnet/csharp/roslyn-sdk/) od wersji 16.8 Preview 3 został wzbogacony o nową funkcjonalność: generator kodu źródłowego, który umożliwia generację fragmentów kodu źródłowego podczas kompilowania projektu. Potencjalnie daje to możliwości wyeliminowanie konieczności kodowania wielu powtarzalnych schematów, chociażby implementacji takich interejsów jak [*IEquatable*](https://docs.microsoft.com/dotnet/api/system.iequatable-1?view=net-5.0).

Wielkie podziękowania dla Daniela Cazzulino za wpis [How to generate code using Roslyn source generators in real world scenarios](https://www.cazzulino.com/source-generators.html) oraz zespołowi projektu *Roslyn* za [Source Generators Cookbook](https://github.com/dotnet/roslyn/blob/master/docs/features/source-generators.cookbook.md).

## Cel

Celem jest stworzenie szablonu dla projektu generatora kodu źródłowego posiadającego następujące cechy:
- możliwość wykorzystywania szablonów tekstowych
- zapis wygenerowanych fragmentów kodu źródłowego do plików
- logowanie pracy generatora
- łatwość uruchamiania i debugowania
- tworzenie pakietu NuGet zawierającego niezbędne zależności

## Napotkane problemy

- Zależności - w normalnych projektach, które budują bibliotekę klas, zależności - zwykle dostarczane w postaci pakietów NuGet - podczas debugowania są ładowane przez śrdowisko uruchomieniowe z podręcznych magazynów pakietów NuGet. Publikacja natomiast odbywa się w postaci pakietu NuGet, który jest zależny od innych pakietów. W przypadku generatora kodu źródłowego sytuacja jest inna, ponieważ biblioteka implementująca generator jest ładowana podczas pracy kompilatora i system musi być w stanie załadować zależności w standardowy sposób bez wsparcia ze strony środowiska uruchomieniowego.
- <a name="initialize"></a>Inicjalizacja - podczas wywoływania metody [*ISourceGenerator.Initialize*](https://docs.microsoft.com/dotnet/api/microsoft.codeanalysis.isourcegenerator.initialize?view=roslyn-dotnet) niedostępne są opcje kompilacji. 
- Debugowania - używanie mechanizmu *Debugger.Launch()* w celu debugowania jest niewygodne i potrafi sprawiać problemy.
- Testy jednostkowe - wymagają stworzenia dodatkowej infrastruktury.
- Pakiet NuGet - najprostszym rozwiązaniem problemu z zależnościami wydaje się umieszczenie wszystkich asemblacji w jednym podkatalogu z biblioteką generatora kodu żródłowego. Niestety w takim przypadku po instalaccji pakietu w projekcie konsumującym generator wszystkie asemblacje pojawią się jako *Analizatory*.

## Struktura solucji

```
    Senga.CodeGenerator
    │
    ├ src
    │  │
    │  ├ Senga.CodeGenerator
    │  ├ Senga.CodeGenerator.Common
    │  └ Senga.CodeGenerator.Pack
    │
    └ Tests
       │
       ├ TestCodeGeneratorConsumer
       └ TestCodeGenerators
```

- **Senga.CodeGenerator** - właściwy kod generatora; implementacje [*ISourceGenerator*](https://docs.microsoft.com/dotnet/api/microsoft.codeanalysis.isourcegenerator?view=roslyn-dotnet).
- **Senga.CodeGenerator.Common** - elementy wspólne wykorzystywane przez właściwy generator oraz przez projekty konsumujące generator; na przykład klasy [Atrybutów](https://docs.microsoft.com/dotnet/csharp/programming-guide/concepts/attributes/).
- **Senga.CodeGenerator.Pack** - projekt odpowiedzialny za stworzenie pakietu NuGet. Nie zawiera żadnych implementacji.
- **TestCodeGeneratorConsumer** - projekt umożliwiający testowanie poprawnego funkcjonowania pakietu NuGet.
- **TestCodeGenerators** - projekt testów jednostkowych.

## Przyjęte rozwiązania

Generator wykorzystuje biblioteki [Serilog](https://serilog.net/) do logowania własnej aktywności.

[*EqualsImplGenerator*](src/Senga.CodeGenerator/Generators/EqualsImplGenerator.cs) do generowania kodu źródłowego wykorzystuje projekt [Scriban](https://github.com/scriban/scriban) (szablony tekstowe).

[*GeneratorBase*](src/Senga.CodeGenerator/Generators/GeneratorBase.cs) dziedziczy po klasie [LogBase](src/Senga.CodeGenerator/Helpers/LogBase.cs) i implementuje abstrakcyjną klasę bazową dla generatorów. Jej głównym zadaniem jest prawidłowe dostarczenie mechanizmu logowania zdarzeń. Po pierwsze wymaga to odczytania ścieżki logowania z przekazanych ustawień kompilacji projektu konsumującego generator i przekazanie jej do *LoggerHelpers.LogPath* co, gdy wydarza się po raz pierwszy po załadowaniu asemblacji, powoduje zmianę sposobu logowania z logowania do pamięci na logowanie do pliku (użycie logowania do pamięci jest spowodowane brakiem dostępu do opcji kompilacji podczas [inicjalizacji](#initialize)). Po drugie rozwiązania problemu związanego ze sposobem ładowania generatorów do pamięci. Ponieważ sposób w jaki generatory są wykorzystywane wyklucza użycie referencji projektu (wszystkie zależności muszą być dostarczone wraz z generatorem i w scenariuszu bazowym znaleźć się w jednym katalogu wraz z biblioteką klas implementującą generator). Przyjęcie takiego rozwiązania powoduje, że wszystkie asemblacje pojawiają się w projekcie wykorzystującym generator jako *Analizatory* (*Dependencies/Analyzer*). Mało tego podczas kompilacji wszystkie te biblioteki będą sprawdzane na okoliczność występowania klas z atrybutem [*[Generator]*](https://docs.microsoft.com/en-us/dotnet/api/microsoft.codeanalysis.generatorattribute?view=roslyn-dotnet). W związku z powyższym lepszym rozwiązaniem jest umieszczenie zależności w odrębnym katalogu - który nie jest podkatalogiem katalogu *Analyzers* - w pakiecie NuGet oraz zaimplementowanie statycznej klasy [*AssemblyResolve*](src/Senga.CodeGenerator/Helpers/AssemblyResolve.cs) dostarczającej obsługę zdarzenia [*AssemblyResolve*](https://docs.microsoft.com/dotnet/api/system.appdomain.assemblyresolve?view=net-5.0) w celu zaladowania bibliotek. Obsługa zdarzenia musi zostać zainstalowana zanim dojdzie do próby załadowania jakiegokolwiek niestandardowego typu, co spowoduje błąd nieodnalezienia biblioteki lub jej zależności. Obsługa instalowana jest w konstruktorze statycznym, który w takiej konfiguracji wykonywany jest najwcześniej. Gdyby w tej klasie znalazły się definicje pól lub właściwości wykorzystujące niestandardowe biblioteki (na przykład logowania) ładowanie nie powiodło by się. W sytuacji, gdy tego typu implementacja została wyniesiona do klasy bazowej problem nie istnieje.

[*GeneratorHelpers*](src/Senga.CodeGenerator/Helpers/GeneratorHelpers.cs) implementuje metody pomocnicze umożliwiające między innymi:
- dostęp do opcji kompilacji projektu konsumującego generator
- zapis plików - w zależności od opcji kompilacji - z wygenerowanymi fragmentami kodu źródłowego

[*Senga.CodeGenerator.csproj*](src/Senga.CodeGenerator/Senga.CodeGenerator.csproj) posiada tylko dwie modyfikacje w stosunku do domyślnego projektu tworzonego przez VS:
- właściwość *IsPackable* ustawioną na *false* - projekt nie bierze udziału w budowaniu pakietu NuGet
- właściwość *CopyLocalLockFileAssemblies* ustawioną na *true* - wszystkie asemblacje zależności, z wyjątkiem [bibliotek uruchomieniowych](#runtimes), są kopiowane do katalogu wynikowego; referencje do projektu dodawane są standardowo z poziomu poleceń VS.

[*Senga.CodeGenerator.props*](src/Senga.CodeGenerator/Senga.CodeGenerator.props) definiuje dodatkowe właściwości kompilacji dostępne z poziomu generatora kodu źródłowego:
- *ProjectDir* - właściwość standardowo dostępna podczas kompilacji
- *DebugSourceGenerators* - decyduje o aktywowaniu debugera w kodzie generatora; domyślna wartość *false*
- *SaveSourceGeneratorSources* - decyduje o włączeniu zapisu wygenerowanych fragmentów kodów źródłowych do pliku; domyślna wartość *true*
- *SourceGeneratorSourcePath* - definiuje względną ścieżkę, do której zapisywane są fragmenty wygnerowanych kodów źródłowych, o ile ta opcja została aktywowana; wartość domyślna: *Generated\\*
- *SourceGeneratorSourcesDir* - pełna ścieżka, do której zapisywane są fragmenty wygnerowanych kodów źródłowych, o ile ta opcja została aktywowana; jest kombinowana ze ścieżek zdefiniowanych w *IntermediateOutputPath* oraz *SourceGeneratorSourcePath*; przykład: dla projektu w katalogu *D:\TestProject*, konfiguracji *Debug*, dla docelowej wersji Framework .Net 5.0 oraz domyślnej wartości *SourceGeneratorSourcePath* będzie to *D:\TestProject\obj\Debug\net5.0\Generated*.

[*Senga.CodeGenerator.Common.csproj*](src/Senga.CodeGenerator.Common/Senga.CodeGenerator.Common.csproj) jest standardowym projektem z właściwością *IsPackable* ustawioną na *false* - projekt nie bierze udziału w budowaniu pakietu NuGet.

[*Senga.CodeGenerator.Pack.csproj*](src/Senga.CodeGenerator.Pack/Senga.CodeGenerator.Pack.csproj) definiuje właściwości pakietu NuGet.

<a name="pack"></a>[*Directory.Build.props*](src/Senga.CodeGenerator.Pack/Directory.Build.props) jest niezbędny w każdej solucji bazującej na proponowanym szablonie. Efektem zdefiniowanych operacji powinno być zbudowanie pakietu o następującej architekturze:

```
    NuGet Pack
    │
    ├ build
    │  │
    │  └ Senga.CodeGenerator.props
    │
    ├ lib
    │  │
    │  └ netstandard2.0
    │     │
    │     ├ <Pliki wynikowe projektu 
    │     ├ Senga.CodeGenerator.Common>
    │     └ ...
    │
    ├ analyzers
    │   │
    │   └ dotnet
    │      │
    │      └ cs
    │         │
    │         └ Senga.CodeGenerator.dll
    │
    └ assemblies
        │
        ├ <Wszystkie pliki i podkatalogi z katalogu 
        ├ wynikowego projektu Senga.CodeGenerator z 
        ├ wyjątiem biblioteki Senga.CodeGenerator.dll>
        └ ...
```

[*TestCodeGeneratorConsumer.csproj*](Tests/TestCodeGeneratorConsumer/TestCodeGeneratorConsumer.csproj) projekt testowy wykorzystujący utworzony pakiet NuGet.

[*CodeGenerators_Test*](Tests/TestCodeGenerators/CodeGeneratorsTests.cs) implementuje metodę *CompileAndLoadAssembly*, która przeprowadza kompilację przekazanego kodu źródłowego z wykorzystaniem generatorów. Podczas kompilacji wykorzystywana jest również klasa [*AnalyzerConfigOptionsProviderForTest*](Tests/TestCodeGenerators/Helpers/AnalyzerConfigOptionsProviderForTest.cs), która dostarcza opcje kompilacji. W tym wypadku umożliwia to pełne przetestowanie mechanizmu logowania i zapisu plików z wygenerowanymi fragmentami kodu źródłowego. Zastosowanie tych mechanizmów nie tylko umożliwia przetestowanie generatorów, ale również dostarcza narzędzia do debugowania.